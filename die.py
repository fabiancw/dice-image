from random import choice
from numpy import zeros, uint8


class Die:
    MARGIN = 1
    DOT = 2
    SIZE = 4*MARGIN + 3*DOT 

    def __init__(self, initial_value=1, end_value=6, steps=10, x=0, y=0):
        self.value = initial_value
        self.end_value = end_value
        self.steps = steps
        self.step = 0
        self.x = x*self.SIZE
        self.y = y*self.SIZE

    def pixels(self):
        """Creates a grid of 9x9 with the dots' positions"""
        if self.value == 1:
            return [[1, 1]]
        elif self.value == 2:
            return [[0, 0], [2, 2]]
        elif self.value == 3:
            return [[0, 0], [1, 1], [2, 2]]
        elif self.value == 4:
            return [[0, 0], [2, 0], [0, 2], [2, 2]]
        elif self.value == 5:
            return [[0, 0], [2, 0], [1, 1], [0, 2], [2, 2]]
        elif self.value == 6:
            return [[0, 0], [1, 0], [2, 0], [0, 2], [1, 2], [2, 2]]

    def draw(self):
        """Creates a numpy array with the die drawing"""
        canvas = zeros((self.SIZE, self.SIZE, 3), dtype=uint8)
        margin = self.MARGIN
        dot = self.DOT
        pixels = self.pixels()

        for px in pixels:
            row = (margin + dot) * px[0] + margin
            col = (margin + dot) * px[1] + margin
            canvas[row:row + dot, col:col+dot, :] = 1

        canvas[0, :, :] = 150
        canvas[-1, :, :] = 150
        canvas[:, 0, :] = 150
        canvas[:, -1, :] = 150
        canvas[canvas == 0] = 255
        canvas[canvas == 1] = 0
        return canvas

    def roll(self):
        """Roll the die. If it reached the final value, returns True"""
        if self.step < self.steps - 1:
            self.value = choice([v for v in range(1, 7) if v != self.value])
            self.step += 1
            return False
        elif self.step == self.steps - 1:
            self.value = choice([v for v in range(1, 7) if v != self.value
                                 and v != self.end_value])
            self.step += 1
            return False
        else:
            self.value = self.end_value
            return True


if __name__ == '__main__':
    d = Die(end_value=5)
    print(d.value)
    while(not d.roll()):
        print(d.value)
    print(d.value)
    print(d.draw()[:, :,  0])
    print(d.draw().shape)
