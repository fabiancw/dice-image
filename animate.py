import cv2
import numpy as np
from die import Die
from draw import load_image, image_to_die, draw_dice
from random import randint


if __name__ == '__main__':
    fname = 'cat.jpg'
    img = load_image(fname, scale=.15)
    img_dice = image_to_die(img)

    width = img_dice.shape[1]
    height = img_dice.shape[0]

    dice = []
    for row in range(height):
        for col in range(width):
            steps = randint(5, 50)
            dice.append(Die(end_value=img_dice[row, col], steps=steps,
                            x=col, y=row))

    img_new = np.zeros((height * dice[0].SIZE, width * dice[0].SIZE, 3),
                       dtype=np.uint8)

    i = 0
    while not draw_dice(img_new, dice, roll=True):
        cv2.imwrite('dice%04d_' % i + fname, img_new)
        i += 1

