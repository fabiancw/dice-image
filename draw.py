import cv2
from sys import exit
from die import Die
from random import randint


def load_image(img, scale=1, die_size=12):
    """Loads an image using OpenCV, converts it to grayscale and scales it.
    Also removes pixels from the bottom left corner so it's multiple of
    die_size

    :param img: Path to the image
    :param scale: Scale in times of the original size
    :param die_size: size of the die (must be the same from the class
    definition)
    """
    i = cv2.imread(img)
    ig = cv2.cvtColor(i, cv2.COLOR_BGR2GRAY)
    return cv2.resize(ig, (int(ig.shape[1] * scale // die_size * die_size),
                           int(ig.shape[0] * scale // die_size * die_size)))


def image_to_die(img):
    """Convert image to an array of dice. Each value corresponds to a value
    from a die

    :param img: Image array
    """
    K = 256//5
    img = img // K * K
    for i in range(6):
        img[img == i*K] = 5 - i

    return img + 1


def draw_dice(arr, dice, swapxy=True, roll=False):
    """Writes an array with every dice

    :param arr: Input array
    :param dice: Dice array
    :param swapxy: True if y goes before x
    :param roll: True if every die rolls after drawing
    """
    done = True
    for d in dice:
        if swapxy:
            arr[d.y:d.y + d.SIZE, d.x:d.x + d.SIZE] = d.draw()
        else:
            arr[d.x:d.x + d.SIZE, d.y:d.y + d.SIZE] = d.draw()
        if roll:
            if not d.roll():
                done = False

    return done


if __name__ == '__main__':
    import pygame
    pygame.init()

    img = load_image('cat.jpg', scale=.15)
    img_dice = image_to_die(img)

    width = img_dice.shape[1]
    height = img_dice.shape[0]

    dice = []
    for row in range(height):
        for col in range(width):
            steps = randint(5, 50)
            dice.append(Die(end_value=img_dice[row, col], steps=steps,
                            x=col, y=row))

    screen = pygame.display.set_mode((width * dice[0].SIZE,
                                      height * dice[0].SIZE))
    pygame.mouse.set_visible(0)
    clock = pygame.time.Clock()
    while True:
        clock.tick(60)
        screen.fill((255, 255, 255))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit(0)

        arr = pygame.surfarray.pixels3d(screen)

        draw_dice(arr, dice, swapxy=False, roll=True)

        pygame.display.flip()
