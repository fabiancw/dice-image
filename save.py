import cv2
import numpy as np
from die import Die
from draw import load_image, image_to_die, draw_dice


if __name__ == '__main__':
    fname = 'cat.jpg'
    img = load_image(fname, scale=.15)
    img_dice = image_to_die(img)

    width = img_dice.shape[1]
    height = img_dice.shape[0]

    dice = []
    for row in range(height):
        for col in range(width):
            dice.append(Die(initial_value=img_dice[row, col],
                            x=col, y=row))

    img_new = np.zeros((height * dice[0].SIZE, width * dice[0].SIZE, 3),
                       dtype=np.uint8)
    draw_dice(img_new, dice)

    cv2.imwrite('dice_' + fname, img_new)
